#!/usr/bin/python3

import tkinter
import time
import numpy as np
import os

__author__ = 'N_Lavrentyeva'

def drawState(curArray):
    os.system('clear')
    for i in range(len(curArray)):
        for j in range(len(curArray[i])):
            if curArray[i][j] == 1:
                canv.create_rectangle(i * widthElement, j * widthElement, (i + 1) * widthElement, (j + 1) * widthElement, fill='blue', outline='black')
            else:
                canv.create_rectangle(i * widthElement, j * widthElement, (i + 1) * widthElement, (j + 1) * widthElement, fill='white', outline='black')
    root.update()


# есть два массива: текущее состояние и будущее. Сначала рандомно генерим начальные условия
numberElements = 25
currentState = np.random.randint(0, 2, numberElements ** 2)
currentState.resize(numberElements, numberElements)
nextState = np.zeros((numberElements, numberElements))
isEnd = False

# пилим окошко:
boardWidth = 500
root = tkinter.Tk()
root.title('Игра "Жизнь"')
rootSize = str(boardWidth) + 'x' + str(boardWidth)
root.geometry(rootSize)
canv = tkinter.Canvas(root,width=boardWidth,height=boardWidth,bg="lightblue")
canv.pack()
widthElement = int(round(boardWidth / numberElements))
# получили начальное состояние
# теперь высчитываем текущее при условии того, что фигура у нас - тор
while not isEnd:
    for i in range(numberElements):
        for j in range(numberElements):
            # считаем количество соседей у клетки
            numberNeighbors = 0
            if currentState[(i - 1) % numberElements][(j - 1) % numberElements] != 0:
                numberNeighbors += 1
            if currentState[(i - 1) % numberElements][j] != 0:
                numberNeighbors += 1
            if currentState[(i - 1) % numberElements][(j + 1) % numberElements] != 0:
                numberNeighbors += 1
            if currentState[i][(j - 1) % numberElements] != 0:
                numberNeighbors += 1
            if currentState[i][(j + 1) % numberElements] != 0:
                numberNeighbors += 1
            if currentState[(i + 1) % numberElements][(j - 1) % numberElements] != 0:
                numberNeighbors += 1
            if currentState[(i + 1) % numberElements][j] != 0:
                numberNeighbors += 1
            if currentState[(i + 1) % numberElements][(j + 1) % numberElements] != 0:
                numberNeighbors += 1

            # получаем новую схему
            if numberNeighbors == 2:
                if currentState[i][j] == 1:
                    nextState[i][j] = 1
                else:
                    nextState[i][j] = 0
            elif numberNeighbors == 3:
                nextState[i][j] = 1
            else:
                nextState[i][j] = 0
    drawState(currentState)
    if np.array_equal(currentState, nextState):
        isEnd = not isEnd
    else:
        currentState = nextState
        nextState = np.zeros((numberElements, numberElements))
    time.sleep(1)
print('End!')
root.mainloop()
